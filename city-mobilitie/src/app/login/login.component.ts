import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css',
              '../../vendor/fontawesome-free/css/all.min.css',
              '../../css/sb-admin.css'
              ]
})
export class LoginComponent implements OnInit {

  show = 0;

  loginForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder, private http:HttpClient, 
    private router: Router) { }

  ngOnInit() {

    var user =JSON.parse(localStorage.getItem('user'));
    if (user){
      this.router.navigate(['home']); 
    }
  
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }

  onFormSubmit() {
    //this.show = 1; 
    if (this.show==0){
      const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      let body = new HttpParams();
      body = body.set('email', this.loginForm.get('email').value);
      body = body.set('password', this.loginForm.get('password').value);
      this.http.post('http://localhost:3000/users/login', body, {headers: myheader}).subscribe(data=>{      
          if (data){
            localStorage.setItem('user', JSON.stringify(data));
            this.router.navigate(['home']);  
          }
        }    
      );
    }
    else if (this.show==1){
      const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
      let body = new HttpParams();
      body = body.set('firstName', this.loginForm.get('firstName').value);
      body = body.set('lastName', this.loginForm.get('lastName').value);
      body = body.set('username', this.loginForm.get('username').value);
      body = body.set('email', this.loginForm.get('email').value);
      body = body.set('password', this.loginForm.get('password').value);
      body = body.set('type', "1");
      this.http.post('http://localhost:3000/users/createUser', body, {headers: myheader}).subscribe(data=>{      
        if (data){
          localStorage.setItem('user', JSON.stringify(data));
          this.router.navigate(['home']);  
        }
      });
    }
  }

  public register(){
    this.show =1; 
  }
  public loginShow(){
    this.show=0;
  }

  public continueWithoutAccount(){
    this.router.navigate(['home']);
  }
}
