var express = require('express');
var router = express.Router();
const parkingSpotsModel = require('../server/model/parkingModel');


/* GET */
router.route('/allSpots').get((req, res) => {
    parkingSpotsModel.find((error, parkingSpotsModel) => {
    if (error) {
        return next(error)
    } else { 
        res.json(parkingSpotsModel)
    }
   });
});

/* GPost create user. */
router.post('/createSpot', (req, res, next) => {
  var newSpot = {  
    city: req.body.city,
    street:req.body.street
  };  
  var data = new parkingSpotsModel(newSpot);  
  data.save(function (err, data) {
    if (err) { return next(err) }
    
    res.json(201, data)
  })
})


module.exports = router;
