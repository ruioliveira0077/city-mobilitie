var express = require('express');
var router = express.Router();
const vehicleModel = require('../server/model/vehiclesModel');
const historyModel = require('../server/model/historyModel');


/* GET vehicles list. */
router.route('/allvehicles').get((req, res) => {

	if (req.query.search !== undefined &&  req.query.search != "")
	{
		vehicleModel.find({ street:{"$regex":".*"+req.query.search+"*.", "$options":"i"} },(error, vehicleModel) => {
			if (error) {
				return next(error)
			} else {
				res.json(vehicleModel)
			}
		   });
	}
	else
	{	
		vehicleModel.find((error, vehicleModel) => {
			if (error) {
				return next(error)
			} else {
				res.json(vehicleModel)
			}
		   });
	}
});

/* GPost users listing. */
router.post('/createVehicle', (req, res) => {
   // console.log(req); 
  var newVehicle = {  
    city: req.body.city,  
    street: req.body.street,  
    battery: req.body.battery,
    status: 0
  };  

  var data = new vehicleModel(newVehicle);  
  data.save(function (err, data) {
    if (err) { return next(err) }
    res.json(201, data)
  })
});

router.put('/requestVehicle',(req, res) => {
	var ObjectID = require('mongodb').ObjectID;
	vehicleModel.updateOne({ _id: ObjectID(req.body.vehicleId) },{$set: {status : 1}},function (err, data) {
	if (err) { return next(err) }
	res.json(201, data)
		var datetime = new Date();
		var newHistoryVehicle = {  
		userId: req.body.userId,  
		vehicleId: req.body.vehicleId,  
		startDate: datetime,
		status: req.body.status,
		status: 1
		};  
		var data = new historyModel(newHistoryVehicle);  
		data.save(function (err, data) {
		if (err) { return next(err) }
		})
	})
});

router.put('/checkoutVehicle',(req, res) => {
	var ObjectID = require('mongodb').ObjectID;
	vehicleModel.updateOne({ _id: new ObjectID(req.body.vehicleId) },
	{$set: {status : 0, street: req.body.street, city: req.body.city}},function (err, data) {
		if (err) { return next(err) }
		res.json(201, data)
			console.log(req); 
			historyModel.updateOne({ _id: ObjectID(req.body.historyId) },
			{$set: {status:0, endDate:req.body.endDate, price: req.body.price}},function (err, data) {
			if (err) { return next(err) }
		})
	})
});

router.route('/history').get(( query, res) => {	
    historyModel.find({ status:1, vehicleId:query.query.vehicleId },(error, historyModel) => {
    if (error) {
        return next(error)
    } else {
        res.json(historyModel)
    }
   });
   
});

router.route('/allHistory').get(( query, res) => {	
    historyModel.find({ userId:query.query.userId },(error, historyModel) => {
    if (error) {
        return next(error)
    } else {
        res.json(historyModel)
    }
   });
   
});

module.exports = router;
