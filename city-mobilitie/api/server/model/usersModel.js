var mongoose = require('mongoose');

var AutoIncrement = require('mongoose-sequence')(mongoose);

const schema = new mongoose.Schema({
    
    userId: {
        type: Number,
    },

    username: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        unique: true
    },
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    type: { type: String },
    money: { type: Number }
});

schema.plugin(AutoIncrement, {id:'userId_seq',inc_field: 'userId'});

var user = mongoose.model("user", schema, "usersCollection");

module.exports = user;