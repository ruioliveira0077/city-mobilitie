var express = require('express');
var router = express.Router();
const userModel = require('../server/model/usersModel');


/* GET users listing. */
router.route('/allUsers').get((req, res) => {
  userModel.find((error, userModel) => {
    if (error) {
        return next(error)
    } else {
        res.json(userModel)
    }
   });
});

/* GPost create user. */
router.post('/createUser', (req, res, next) => {
  console.log(req); 
  var newUser = {  
    username: req.body.username,
    email:req.body.email,
    password:req.body.password,  
    firstName: req.body.firstName,  
    lastName: req.body.lastName,
    type: req.body.type, 
    money: 50
  };  
  var data = new userModel(newUser);  
  data.save(function (err, data) {
    if (err) { return next(err) }
    
    res.json(201, data)
  })
})


/* LOGIN*/
router.post('/login', (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  try {
    userModel.findOne({
          email: email
      }, function(err, foundUser) {
          if (err || !foundUser) {
               res.redirect.json("/login?err=The username does not exist");
          }
          if (foundUser.password !== password) {
              // you can use  bcryptjs for hashing and comparing hashed values.

               res.redirect.json("/login?err=The username or password is invalid");
          }
          console.log("done")
          res.status(200).json(foundUser);
      });
  } catch (error) {
    res.redirect("/login?err=something went wrong!");
  }
})

router.put('/updateUser',(req, res) => {
  userModel.updateOne({ userId:req.body.userId },
	{$set: {money: req.body.money}},function (err, data) {
		if (err) { 
      console.log(err); 
      return next(err)}
		res.json(201, data)
	})
});

module.exports = router;
