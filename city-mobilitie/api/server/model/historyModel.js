var mongoose = require('mongoose');


const schema = new mongoose.Schema({
    userId: { type: String },
    vehicleId: { type: String },
    startDate: { type: Date },
    endDate: { type: Date },
    status: { type: Number },
    price: { type: Number }
});



var historyModel = mongoose.model("history", schema, "historyCollection");

module.exports = historyModel;