var express = require('express');
var app = express();

let mongoose = require('mongoose');
module.exports = app;

var URL = "mongodb://localhost:27017/apiDb";

//routes
let users = require('./routes/users');
let vehicles = require('./routes/vehicles')
let parking = require('./routes/parkingSpots')
bodyParser = require('body-parser')
path = require('path'),
cors = require('cors'),

// Setting up express
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cors()); 
app.use('/users',users);
app.use('/vehicles',vehicles);
app.use('/parking',parking);
app.disable('etag');


mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useUnifiedTopology', false);

//Connection establishment
mongoose.Promise = global.Promise;
mongoose.connect(URL, {
    useNewUrlParser: true,
    dbName: "myDb"
}).then(() => {
        console.log('Database connected')
    },
    error => {
        console.log('Database could not be connected : ' + error)
    }
)


const port = process.env.PORT || '3000';
app.set('port', port);

app.listen(port, () => console.log(`API running on localhost:${port}`));

app.use(express.static(path.join(__dirname, 'dist')));