import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import * as jQuery from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbModal, ModalDismissReasons, NgbModalOptions} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Capability } from 'protractor';


@Component({
    selector: 'app-home',
    templateUrl: 'home.component.html',
    styleUrls: [
            '../../css/sb-admin.css',
            '../../vendor/datatables/dataTables.bootstrap4.css',
            '../../vendor/fontawesome-free/css/all.min.css',
            'home.component.css',
        ]
})
export class HomeComponent implements OnInit {
  //let vm = this;
  user
  vehicles=[];
  spots = []; 
  users = []; 
  allHistory =[]; 
  currentSaving;
  type
  search = ""; 
  currentView = 0; 
  numberOfUsers = 0;
  vehiclesAvailable = 0;
  vehiclesinUse = 0; 
  newSpot = false; 
  newVehicle = false;  
  vehicleForm: FormGroup;
  checkoutForm: FormGroup;
  newSpotForm: FormGroup;
  searchForm: FormGroup; 
  modalOptions:NgbModalOptions;
  closeResult: string;

  constructor(private http:HttpClient, 
    private formBuilder: FormBuilder,
    private router: Router, private modalService: NgbModal) { 
    this.modalOptions = {
        ariaLabelledBy: 'modal-basic-title', 
        size: 'lg', 
        windowClass: 'custom-class'
    }
  }

  public logOut(){
     localStorage.removeItem('user');
     this.router.navigate(['login']);
  }

    public DashboardPage( view){
        this.currentView = view; 
    }

    public register() {
        this.router.navigate(['login']);
    }

    public addSpot(){
        this.newSpot = true; 
    }

    public addVehicle(){
        this.newVehicle = true; 
    }

    public searchVehicles(){
        this.search = this.searchForm.get('search').value; 
        this.getAllVehicles()
        
    }

    public createSpot(){
        const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        let body = new HttpParams();
        body = body.set('city', this.newSpotForm.get('city').value);
        body = body.set('street', this.newSpotForm.get('street').value);        
        this.http.post('http://localhost:3000/parking/createSpot', body, {headers: myheader}).subscribe(data=>{      
            if (data){
                this.newSpot= false; 
                this.getAllSpots();
            }
        });
        
    }

    ngOnInit() {
        this.user =JSON.parse(localStorage.getItem('user'))
        this.type = 2;
        if (this.user){
        this.type = this.user.type; 
        this.currentSaving = this.user.money;
        }
        this.getAllVehicles();
        this.getAllSpots();
        this.getAllUsers();
        this.getHistory();

        this.vehicleForm = this.formBuilder.group({
        city: ['', Validators.required],
        street: ['', Validators.required]
        });

        this.newSpotForm = this.formBuilder.group({
            city: ['', Validators.required],
            street: ['', Validators.required]
            });

        this.checkoutForm = this.formBuilder.group({
            city: ['', Validators.required],
            street: ['', Validators.required],
            optionSelected: ['', Validators.required]
        });

        this.searchForm = this.formBuilder.group({
            search: ['', Validators.required]
        });
    }

    submitNewVehicle(){
        const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        let body = new HttpParams();
        body = body.set('city', this.vehicleForm.get('city').value);
        body = body.set('street', this.vehicleForm.get('street').value);
        body = body.set('battery', "100");
        this.http.post('http://localhost:3000/vehicles/createVehicle', body, {headers: myheader}).subscribe(data=>{      
            if (data){
            this.getAllVehicles();
            this.getAllSpots();
            this.getAllUsers();
            this.getHistory();
            this.newVehicle=false; 
            }
        });
    }
  
    checkout(content, vehicle ){
        this.modalService.open(content, this.modalOptions).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
            // Request vehicle 
            const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
            let body = new HttpParams();

            var diffMinutes;
            var endDate = new Date();
            var historyId;

            let optionSelected = this.checkoutForm.get('optionSelected').value;

            
            const params = new HttpParams()
            .set('vehicleId', vehicle._id )
            this.http.get<any>('http://localhost:3000/vehicles/history',{params}).subscribe(data => {
                var startDate = new Date(data[0].startDate);
                diffMinutes = Math.round((Math.abs(endDate.getTime() - startDate.getTime()))/1000/60);
                historyId = data[0]._id;

                var price = Math.round(1 + .15*diffMinutes); 
                body = body.set('userId',this.user.userId );
                body = body.set('vehicleId',vehicle._id );
                if(optionSelected != "") 
                {
                    var aux = optionSelected.split(",");
                    body = body.set('city', aux[0]);
                    body = body.set('street',aux[1]);
                    price = price -.50;  
                }
                else 
                {
                    body = body.set('city', this.checkoutForm.get('city').value);
                    body = body.set('street', this.checkoutForm.get('street').value);
                }
                body = body.set('battery',vehicle.battery );
                body = body.set('price', price.toString() );
                body = body.set('historyId', historyId);
                body = body.set('endDate', endDate.toString());
                this.http.put('http://localhost:3000/vehicles/checkoutVehicle', body, {headers: myheader}).subscribe(data=>{      
                    if (data){
                        let bodyUser = new HttpParams();
                        this.user.money = this.user.money - price; 
                        bodyUser = bodyUser.set('userId',this.user.userId );
                        bodyUser = bodyUser.set('money',this.user.money );
                        this.http.put('http://localhost:3000/users/updateUser', bodyUser, {headers: myheader}).subscribe(data=>{      
                            if (data){
                                localStorage.setItem('user', JSON.stringify(this.user));
                                this.currentSaving =  this.user.money; 
                                this.getAllVehicles();
                                this.getAllSpots();
                                this.getAllUsers();
                                this.getHistory();
                            }
                        });    
                    }
                }    
                );
            }); 
            
        
        }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }


open(content, vehicle ){
    this.modalService.open(content, { windowClass : "myCustomModalClass"}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
        // Request vehicle 
        const myheader = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        let body = new HttpParams();
        body = body.set('userId',this.user.userId );
        body = body.set('vehicleId',vehicle._id );
        this.http.put('http://localhost:3000/vehicles/requestVehicle', body, {headers: myheader}).subscribe(data=>{      
            if (data){
                this.getAllVehicles();
                this.getAllSpots();
                this.getAllUsers();
                this.getHistory();
            }
          }    
        );
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
}

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
        } else {
        return  `with: ${reason}`;
        }
    }



    private  getAllVehicles(){

        const params = new HttpParams()
        .set('search', this.search )
        this.http.get<any>('http://localhost:3000/vehicles/allVehicles',{params}).subscribe(data => {
            this.vehicles = data;
            this.vehiclesAvailable = 0; 
            this.vehiclesinUse = 0 ; 
            for( let vehicle of this.vehicles)
            {
                if (vehicle.status == 0 )
                {
                    this.vehiclesAvailable = this.vehiclesAvailable + 1;
                }
                else
                {
                    this.vehiclesinUse == this.vehiclesinUse + 1; 
                }
            }

        })
    }

    private  getAllSpots(){
        this.http.get<any>('http://localhost:3000/parking/allSpots').subscribe(data => {
        this.spots = data;
        })
    }

    private  getAllUsers(){
        this.http.get<any>('http://localhost:3000/users/allUsers').subscribe(data => {
            this.users = data;
            this.numberOfUsers = this.users.length; 
        })
    }

    private  getHistory(){
        if (this.user)
        {
            const params = new HttpParams()
            .set('userId', this.user.userId)
            this.http.get<any>('http://localhost:3000/vehicles/allHistory',{params}).subscribe(data => {
                this.allHistory = data
            });
        }
    }
}
