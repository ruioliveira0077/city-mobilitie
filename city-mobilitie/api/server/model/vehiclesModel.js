var mongoose = require('mongoose');


const schema = new mongoose.Schema({
    /*
    username: {
        type: String,
        required: true,
        unique: true
    },
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },*/
    city: { type: String },
    street: { type: String },
    battery: { type: Number },
    status: { type: Number }
});



var vehicles = mongoose.model("vehicles", schema, "vehiclesCollection");

module.exports = vehicles;